let me = {};
me.firstName = 'Laura';
me.lastName = 'Mayer-Sommer'
me.favoriteFood = 'chocolate'

let mom = {};
mom.firstName = 'Ann';
mom.lastName = 'Mayer-Sommer'
mom.favoriteFood = 'broccoli'

let dad = {};
dad.firstName = 'Bill';
dad.lastName = 'Mayer-Sommer'
dad.favoriteFood = 'garlic'

console.log(mom.firstName);
console.log(dad.favoriteFood);