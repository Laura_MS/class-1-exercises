function formatAddress(addressString)
{
  	var addressArray = [];
  	var name,fName,lName;
 	addressArray = addressString.split('\n');
  
  	//first line
 	name = addressArray[0].split(' ');
 	fName = name[0];
  	lName = name[1];
  
  	//third line
  	var csz = addressArray[2].split(', ');
  	var sz = csz[1].split(' '); //north, dakota, 12345
  	var city = csz[0];
 	var state = sz.slice(0,sz.length-1).join(' ');
	var zip = sz[sz.length-1];
  
	document.write(fName+' '+lName+'<br/>');
	document.write(addressArray[1]+'<br/>');
	document.write(city+', '+state+' '+zip+'<br/>');
}

var addy = "Jane Doe\n 1 Main Street\nNew City, North Dakota 12345";
var addy2 = "Anne Smith\n123 West Road\nFairfax, VA 54421";
formatAddress(addy);
formatAddress(addy2);